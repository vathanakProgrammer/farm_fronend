import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

interface IMenuItem {
  type: string; // Possible values: link/dropDown/icon/separator/extLink
  name?: string; // Used as display text for item and title for separator type
  state?: string; // Router state
  icon?: string; // Material icon name
  tooltip?: string; // Tooltip text
  disabled?: boolean; // If true, item will not be appeared in sidenav.
  sub?: IChildItem[]; // Dropdown items
  badges?: IBadge[];
}
interface IChildItem {
  type?: string;
  name: string; // Display text
  state?: string; // Router state
  icon?: string;
  sub?: IChildItem[];
}

interface IBadge {
  color: string; // primary/accent/warn/hex color codes(#fff000)
  value: string; // Display text
}

@Injectable()
export class NavigationService {
  constructor() { }
  iconMenu: IMenuItem[] = [
    {
      name: 'Dashboard',
      icon: "local_offer",
      type: "dropDown",
      sub: [
        { name: 'Item', state: 'item' }
      ]
    },
    {
      name: "Customer care",
      type: "dropDown",
      tooltip: "Support Ticket",
      icon: "group",
      sub: [
        {
          name: "Support Ticket", state: "support-ticket"
        },
        {
          name: 'Support Ticket Type', state: "support-type"
        },
        {
          name: 'Knowledge', state: 'knowledge'
        }
      ]
    },
    {
      name: 'HSIA Calendar',
      type: "link",
      icon: "calendar_today",
      state: 'hsia-calendar'
    },
    {
      name: 'Bazi Calendar',
      type: "link",
      icon: "calendar_today",
      state: 'bazi-calendar'
    },
    {
      name: 'Event Management',
      type: "link",
      icon: "event",
      state: 'event-management'
    },
    {
      name: 'Product',
      type: "link",
      icon: "domain",
      state: 'product'
    },
    {
      name: 'Plan',
      type: "link",
      icon: "domain",
      state: 'plan'
    },
    {
      name: 'Plan Feature',
      type: "link",
      icon: "domain",
      state: 'plan-feature'
    },
    {
      name: "Customer Info",
      type: "dropDown",
      tooltip: "Customer Info",
      icon: "group",
      sub: [
        {
          name: "Subscription", state: "subscription"
        }
      ]
    },
    {
      name: "Setting",
      type: "dropDown",
      tooltip: 'setting',
      icon: "settings",
      sub: [
        {
          name: "Language", state: "language"
        }
      ]
    }

  ];


  separatorMenu: IMenuItem[] = [
  ];

  plainMenu: IMenuItem[] = [
  ];

  // Icon menu TITLE at the very top of navigation.
  // This title will appear if any icon type item is present in menu.
  iconTypeMenuTitle: string = "Frequently Accessed";
  // sets iconMenu as default;
  menuItems = new BehaviorSubject<IMenuItem[]>(this.iconMenu);
  // navigation component has subscribed to this Observable
  menuItems$ = this.menuItems.asObservable();

  // Customizer component uses this method to change menu.
  // You can remove this method and customizer component.
  // Or you can customize this method to supply different menu for
  // different user type.
  publishNavigationChange(menuType: string) {
    switch (menuType) {
      case "separator-menu":
        this.menuItems.next(this.separatorMenu);
        break;
      case "icon-menu":
        this.menuItems.next(this.iconMenu);
        break;
      default:
        this.menuItems.next(this.plainMenu);
    }
  }
  loadMenu() {

  }
}
