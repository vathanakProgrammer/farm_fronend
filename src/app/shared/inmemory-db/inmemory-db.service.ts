import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ChatDb } from './chat-db';
import { InvoiceDB } from './invoices';
import { Todo, TodoTag } from './todo';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  
  createDb(){
    return {
      'contacts': ChatDb.contacts,
      'chat-collections': ChatDb.chatCollection,
      'chat-user': ChatDb.user,
      'invoices': InvoiceDB.invoices,
      'todoList': Todo.todoList,
      'todoTag': TodoTag.tag
    }
  }
}
