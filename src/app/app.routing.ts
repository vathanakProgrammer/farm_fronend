import { Routes } from '@angular/router';
import { AuthLayoutComponent } from './shared/components/layouts/auth-layout/auth-layout.component';

export const rootRouterConfig: Routes = [
    {
        path: '',
        redirectTo: 'others/blank',
        pathMatch: 'full'
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: 'sessions',
                loadChildren: () => import('./views/sessions/sessions.module').then(m => m.SessionsModule),
                data: { title: 'Session' }
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'sessions/404'
    }
];