
import { Injectable } from "@angular/core";
import { HttpService } from '../core/http/http.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})

export class configservice{
    serverUrl = environment.serverUrl
    constructor(
        private http : HttpClient
    ){}
    geturl(){
       return this.http.get('/student/index')
    }
}
