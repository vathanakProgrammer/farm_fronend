import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SharedMaterialModule } from 'src/app/shared/shared-material.module';
import { FlexLayoutModule } from '@angular/flex-layout'
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar'
import { MatProgressBarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { SessionRoutes } from './others.routing';
@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    MatProgressBarModule,

    RouterModule.forChild(SessionRoutes)
  ],
  declarations: [SigninComponent, SignupComponent]
})
export class SessionsModule { }
