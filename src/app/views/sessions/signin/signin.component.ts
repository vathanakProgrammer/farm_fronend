import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatProgressBar, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  signinForm: FormGroup;
  @ViewChild(MatProgressBar, { static: false }) progressBar: MatProgressBar;
  constructor( 
    private fb: FormBuilder,
    private router: Router,

    private route: ActivatedRoute,
    private snack: MatSnackBar
  ) {
    this.createForm()
  }
  ngOnInit() {
  }

  private createForm() {
    const password = new FormControl('', Validators.required);
    this.signinForm = this.fb.group(
      {
        username: ["", [Validators.required]],
        password: password,
      }
    );
  }

}
