import { Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';

export const SessionRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "signup",
                component: SignupComponent,
                data: { title: "Signup" }
            },
            {
                path: "singnin",
                component: SigninComponent,
                data: { title: "Signin" }
            }
        ]
    }
];